# minggu_10_rest_api

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Output

- API Reqres

![api-reqres](https://user-images.githubusercontent.com/74108522/166132231-e5d6fbcb-723a-4a30-99aa-6bec9ff58116.PNG)

- API Movies

![api-movies](https://user-images.githubusercontent.com/74108522/166132233-15360e9b-3f81-4733-a169-cecb600defbd.PNG)
