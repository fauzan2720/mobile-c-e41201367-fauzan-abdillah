import 'dart:convert';

import 'package:http/http.dart' show Client, Response;
import 'package:minggu_10_rest_api/model/popular_movies.dart';

class ApiProvider {
  String apiKey = '4da9bcea15992c62910a6982258a2cc8';
  String baseUrl = 'https://api.themoviedb.org/3';

  Client client = Client();
  Future<PopularMovies> getPopularMovies() async {
    String url = '$baseUrl/movie/popular?api_key=$apiKey';
    //  print(url);
    Response response = await client.get(Uri.parse(url));

    if (response.statusCode == 200) {
      return PopularMovies.fromJson(jsonDecode(response.body));
    } else {
      throw Exception(response.statusCode);
    }
  }
}
