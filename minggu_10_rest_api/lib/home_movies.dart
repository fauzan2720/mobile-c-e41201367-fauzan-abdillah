import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:minggu_10_rest_api/data/api_provider.dart';
import 'package:minggu_10_rest_api/model/popular_movies.dart';

class HomeMoviesPage extends StatefulWidget {
  const HomeMoviesPage({Key? key}) : super(key: key);

  @override
  State<HomeMoviesPage> createState() => _HomeMoviesPageState();
}

class _HomeMoviesPageState extends State<HomeMoviesPage> {
  ApiProvider apiProvider = ApiProvider();
  late Future<PopularMovies> popularMovies;
  String imageBaseUrl = 'https://image.tmdb.org/t/p/w500';

  @override
  void initState() {
    popularMovies = apiProvider.getPopularMovies();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('E41201367 - Movies App'),
      ),
      body: FutureBuilder(
        future: popularMovies,
        builder: (
          BuildContext context,
          AsyncSnapshot snapshot,
        ) {
          if (snapshot.hasData) {
            print("Has Data: ${snapshot.hasData}");
            return ListView.builder(
              itemCount: snapshot.data.results.length,
              itemBuilder: (BuildContext context, int index) {
                return moviesItem(
                  poster:
                      '$imageBaseUrl${snapshot.data.results[index].posterPath}',
                  title: '${snapshot.data.results[index].title}',
                  date: '${snapshot.data.results[index].releaseDate}',
                  voteAverage: '${snapshot.data.results[index].voteAverage}',
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => MovieDetail(
                          movie: snapshot.data.results[index],
                        ),
                      ),
                    );
                  },
                );
              },
            );
          } else if (snapshot.hasError) {
            print("Has Error: ${snapshot.hasError}");
            return const Text('Error!!!');
          } else {
            print("Loading...");
            return const CircularProgressIndicator();
          }
        },
      ),
    );
  }

  Widget moviesItem({
    required String poster,
    required String title,
    required String date,
    required String voteAverage,
    required Function()? onTap,
  }) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.all(10),
        child: Card(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                width: 120,
                child: CachedNetworkImage(
                  imageUrl: poster,
                ),
              ),
              const SizedBox(width: 20),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(top: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        title,
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          const Icon(
                            Icons.calendar_today,
                            size: 12,
                          ),
                          const SizedBox(width: 5),
                          Text(date),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          const Icon(
                            Icons.star,
                            size: 12,
                          ),
                          const SizedBox(width: 5),
                          Text(voteAverage),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class MovieDetail extends StatelessWidget {
  final Results movie;

  const MovieDetail({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(movie.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Text(movie.overview),
      ),
    );
  }
}
