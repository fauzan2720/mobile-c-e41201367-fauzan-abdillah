import 'package:flutter/material.dart';
import 'package:minggu_10_rest_api/home_movies.dart';
import 'package:minggu_10_rest_api/reqris_data/get_data.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'E41201367 - Fauzan Abdillah',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      // home: const GetDataScreenPage(),
      home: const HomeMoviesPage(),
    );
  }
}
