import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:minggu_10_rest_api/reqris_data/get_detail_screen.dart';

class GetDataScreenPage extends StatefulWidget {
  const GetDataScreenPage({Key? key}) : super(key: key);

  @override
  State<GetDataScreenPage> createState() => _GetDataScreenPageState();
}

class _GetDataScreenPageState extends State<GetDataScreenPage> {
  final String url = 'https://reqres.in/api/users?page=2';
  List? data;

  @override
  void initState() {
    _getRefreshData();
    super.initState();
  }

  Future<void> _getRefreshData() async {
    getJsonData(context);
  }

  Future<void> getJsonData(BuildContext context) async {
    var response =
        await http.get(Uri.parse(url), headers: {'Accept': 'application/json'});
    print(response.body);

    setState(() {
      var convertDataToJson = jsonDecode(response.body);
      data = convertDataToJson['data'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('E41201367 - API Reqres'),
      ),
      body: RefreshIndicator(
        onRefresh: _getRefreshData,
        child: data == null
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: data == null ? 0 : data!.length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => GetDataDetailScreenPage(
                                value: data![index]['id'],
                              ),
                            ),
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 16,
                            vertical: 6,
                          ),
                          child: Row(
                            children: [
                              ClipRRect(
                                child: Image.network(
                                  data![index]['avatar'],
                                  height: 80,
                                  width: 80,
                                ),
                              ),
                              const SizedBox(width: 10),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    data![index]['first_name'] +
                                        ' ' +
                                        data![index]['last_name'],
                                  ),
                                  Text(
                                    data![index]['email'],
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      const Divider(),
                    ],
                  );
                },
              ),
      ),
    );
  }
}
