import 'dart:io';

void main() {
  stdout.write("Yakin ingin menginstall aplikasi? (y/t) : ");
  var ternaryCondition = stdin.readLineSync();

  print((ternaryCondition == 'y') // kondisi
      ? 'Anda akan menginstall aplikasi dart' // jika bernilai true
      : 'Aborted'); // jika bernilai false
}
