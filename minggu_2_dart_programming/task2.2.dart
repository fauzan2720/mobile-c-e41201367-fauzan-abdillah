import 'dart:io';

void main() {
  stdout.write('Masukkan nama : ');
  var nama = stdin.readLineSync();

  stdout.write('Masukkan peran Anda : ');
  var peran = stdin.readLineSync();

  if (nama == '') { // jika inputan nama kosong
    print('Nama wajib diisi!');
  } else if (peran == '') { // jika inputan peran kosong
    print('Halo $nama, Pilih peranmu untuk memulai game!');
  } else if (peran == 'Penyihir') {
    print(
        'Selamat datang di Dunia Werewolf, $nama\nHalo $peran $nama, kamu dapat melihat siapa yang menjadi werewolf!');
  } else if (peran == 'Guard') {
    print(
        'Selamat datang di Dunia Werewolf, $nama\nHalo $peran $nama, kamu akan membantu melindungi temanmu dari serangan werewolf');
  } else if (peran == 'Werewolf') {
    print(
        'Selamat datang di Dunia Werewolf, $nama\nHalo $peran $nama, Kamu akan memakan mangsa setiap malam!');
  } else {
    print('Halo $nama, Pilih peranmu dengan benar untuk memulai game!');
  }
}
