void main() {
  int day = 27;
  int month = 5;
  int year = 2001;

  if (day >= 1 && day <= 31) { // mengecek tanggal
    if (year >= 1900 && year <= 2200) { // mengecek tahun
      switch (month) { // mengecek bulan
        case 1:
          print('$day Januari $year');
          break;
        case 2:
          print('$day Februari $year');
          break;
        case 3:
          print('$day Maret $year');
          break;
        case 4:
          print('$day April $year');
          break;
        case 5:
          print('$day Mei $year');
          break;
        case 6:
          print('$day Juni $year');
          break;
        case 7:
          print('$day Juli $year');
          break;
        case 8:
          print('$day Agustus $year');
          break;
        case 9:
          print('$day September $year');
          break;
        case 10:
          print('$day Oktober $year');
          break;
        case 11:
          print('$day November $year');
          break;
        case 12:
          print('$day Desember $year');
          break;
        default:
          print('Format month tidak sesuai!');
          break;
      }
    } else {
      print('Format year tidak sesuai!');
    }
  } else {
    print('Format day tidak sesuai!');
  }
}
