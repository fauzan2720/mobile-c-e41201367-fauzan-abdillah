void main() {
  for (var i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 == 1) { // jika kelipatan 3 dan ganjil
      print('$i - I Love Coding');
    } else {
      print((i % 2 == 1) ? '$i - Santai' : '$i - Berkualitas');
    }
  }
}
