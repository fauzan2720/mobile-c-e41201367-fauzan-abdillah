void main() {
  var staircase = ""; // nilai awal kosong (string)

  // var = i;
  // i = 3, staircase = ##
  // while (i <= 7) {
  //   staircase = staircase + '#'; // ###
  //   print(staircase);
  //   i++;
  // }

  for (var i = 1; i <= 7; i++) {
    staircase = staircase + '#'; // merubah nilai berdasarkan string yang ditambahkan (#)
    print(staircase);
  }
}
