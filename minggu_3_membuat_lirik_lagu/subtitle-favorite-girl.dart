void main() {
  print('----- Justine Bieber - Favorite Girl -----');
  print('Song: https://www.youtube.com/watch?v=mKbPxUo7nrQ');

  Future.delayed(
    Duration(seconds: 1), // memberi jeda
    () => print('\nReady, Song!\n'),
  );

  Future.delayed(
    Duration(seconds: 2),
    () => print('You\'re who I\'m thinking of'),
  );

  Future.delayed(
    Duration(seconds: 7),
    () => print('Girl, you ain\'t my runner-up'),
  );

  Future.delayed(
    Duration(seconds: 10),
    () => print('And no matter what you\'re always number one'),
  );

  Future.delayed(
    Duration(seconds: 13),
    () => print('My prized possession, One and only'),
  );

  Future.delayed(
    Duration(seconds: 16),
    () => print('Adore ya. Girl, I want ya'),
  );

  Future.delayed(
    Duration(seconds: 19),
    () => print('The one I can\'t live without'),
  );

  Future.delayed(
    Duration(seconds: 22),
    () => print('That\'s you!!'),
  );

  Future.delayed(
    Duration(seconds: 24),
    () => print('That\'s you!'),
  );
}
