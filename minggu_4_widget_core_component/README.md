# Mobile-C-E41201367-Fauzan Abdillah

Membuat Componen Widget dan Telegram App

NIM : E41201367

Nama : Fauzan Abdillah

1. Sketsa Aplikasi

![Sketsa Aplikasi](https://user-images.githubusercontent.com/74108522/158956844-34931157-074a-4eef-ae44-c7df605579eb.jpeg)


2. Wireframe/Mock Up Aplikasi

![Wireframe](https://user-images.githubusercontent.com/74108522/158956848-2cf19107-5fb4-44df-92c6-5366f1e77102.jpeg)


3. Component Widget

![ComponentWidget](https://user-images.githubusercontent.com/74108522/158961106-e4fa59eb-1f86-4740-b1cc-64f58f489ccd.gif)


4. Telegram App

![TelegramApp](https://user-images.githubusercontent.com/74108522/158961123-6e77c822-e34a-4b52-ad54-c1f971495c84.gif)