import 'package:flutter/material.dart';
import 'package:minggu_4_widget_core_component/Tugas12/drawer_screen.dart';
import 'package:minggu_4_widget_core_component/Tugas12/Models/chart_model.dart';

class TelegramPage extends StatefulWidget {
  const TelegramPage({Key? key}) : super(key: key);

  @override
  State<TelegramPage> createState() => _TelegramPageState();
}

class _TelegramPageState extends State<TelegramPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Telegram'),
        actions: const [
          Padding(
            padding: EdgeInsets.all(8),
            child: Icon(Icons.search),
          ),
        ],
      ),
      drawer: DrawerScreenPage(),
      body: ListView.separated(
          itemBuilder: (ctx, i) {
            return ListTile(
              // leading: Image.network(
              //   items[1].profileUrl.toString(),
              // ),
              leading: CircleAvatar(
                radius: 28,
                backgroundImage: NetworkImage(
                  items[i].profileUrl.toString(),
                ),
              ),
              title: Text(
                items[i].name.toString(),
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                items[i].message.toString(),
              ),
              trailing: Text(
                items[i].time.toString(),
              ),
            );
          },
          separatorBuilder: (ctx, i) {
            return const Divider();
          },
          itemCount: items.length),
      floatingActionButton: FloatingActionButton(
        child: const Icon(
          Icons.create,
          color: Colors.white,
        ),
        backgroundColor: const Color(0xff65a9e0),
        onPressed: () {},
      ),
    );
  }
}
