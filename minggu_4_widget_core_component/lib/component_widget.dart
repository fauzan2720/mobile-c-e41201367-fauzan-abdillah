import 'package:flutter/material.dart';

class ComponentWidgetPage extends StatelessWidget {
  const ComponentWidgetPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget text() {
      return const Text(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel convallis massa. Donec eget pellentesque nisl, vel pellentesque risus. Mauris quis velit scelerisque lectus maximus tristique. Vestibulum mollis ipsum ac metus suscipit bibendum. Maecenas eu ultricies arcu, vitae blandit nunc. Nullam ex nunc, consectetur eget quam vel, tincidunt congue ligula. Sed finibus enim vel congue ultricies.",
        style: TextStyle(
          color: Colors.white,
          backgroundColor: Colors.black,
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
        ),
      );
    }

    Widget icon() {
      return Container(
        margin: const EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 30,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: const [
                Icon(Icons.home),
                SizedBox(height: 6),
                Text('Beranda'),
              ],
            ),
            Column(
              children: const [
                Icon(Icons.store),
                SizedBox(height: 6),
                Text('Belanja'),
              ],
            ),
            Column(
              children: const [
                Icon(Icons.settings),
                SizedBox(height: 6),
                Text('Pengaturan'),
              ],
            ),
          ],
        ),
      );
    }

    Widget button() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextButton(
            child: const Text("This is TextButton Button"),
            onPressed: () {},
          ),
          const SizedBox(height: 15),
          RaisedButton(
            color: Colors.blue,
            child: const Text("This is Raised Button"),
            onPressed: () {},
          ),
          const SizedBox(height: 15),
          MaterialButton(
            color: Colors.yellow,
            child: const Text("This is Material Button"),
            onPressed: () {},
          ),
          const SizedBox(height: 15),
          FlatButton(
            color: Colors.green,
            child: const Text("This is FlatButton Button"),
            onPressed: () {},
          ),
          const SizedBox(height: 15),
          const Text("This is FloatingActionButton Button"),
          FloatingActionButton(
            child: const Icon(Icons.shopping_bag),
            backgroundColor: Colors.purple,
            onPressed: () {},
          ),
        ],
      );
    }

    Widget textformfield() {
      return Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 40,
          vertical: 10,
        ),
        child: Form(
          child: Column(
            children: [
              TextFormField(
                decoration: const InputDecoration(hintText: "Email"),
              ),
              TextFormField(
                obscureText: true,
                decoration: const InputDecoration(hintText: "Kata Sandi"),
              ),
              const SizedBox(height: 20),
              Container(
                width: double.infinity,
                height: 50,
                decoration: BoxDecoration(
                  color: const Color(0xffD3D3D3),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: TextButton(
                  child: const Text("Masuk"),
                  onPressed: () {},
                ),
              ),
            ],
          ),
        ),
      );
    }

    Widget imageurl() {
      return Expanded(
        child: Center(
          child: Image.network(
            'https://nordot-res.cloudinary.com/c_limit,w_800,f_auto,q_auto:eco/ch/images/696686921651455073/origin_1.jpg',
          ),
        ),
      );
    }

    Widget imagelocal() {
      return Expanded(
        child: Center(
          child: Image.asset('assets/img/kanjiro.jpg'),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('E41201367 - Component Widget'),
        actions: [
          IconButton(
            icon: Image.asset('assets/img/profile.jpg'),
            tooltip: 'Fauzan Abdillah',
            onPressed: () {},
          ),
        ],
      ),
      body: ListView(
        children: [
          const SizedBox(height: 20),
          const Text('1. Penerapan Text Widget:'),
          text(),
          const Divider(),
          const Text('2. Penerapan Icon Widget:'),
          icon(),
          const Divider(),
          const Text('3. Penerapan Button Widget:'),
          button(),
          const Divider(),
          const Text('4. Penerapan Text Form Field Widget:'),
          textformfield(),
          const Divider(),
          const Text('5. Menampilkan Gambar dari URL:'),
          imageurl(),
          const Divider(),
          const Text('6. Menampilkan Gambar dari Lokal:'),
          imagelocal(),
        ],
      ),
    );
  }
}
