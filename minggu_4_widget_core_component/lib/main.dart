import 'package:flutter/material.dart';
import 'package:minggu_4_widget_core_component/Tugas12/telegram.dart';
import 'package:minggu_4_widget_core_component/component_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      // home: ComponentWidgetPage(),
      home: TelegramPage(),
    );
  }
}
