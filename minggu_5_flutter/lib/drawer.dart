import 'package:flutter/material.dart';

class DrawerScreenPage extends StatefulWidget {
  @override
  _DrawerScreenPageState createState() => _DrawerScreenPageState();
}

class _DrawerScreenPageState extends State<DrawerScreenPage> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          const UserAccountsDrawerHeader(
            decoration: BoxDecoration(
              color: Colors.red,
            ),
            accountName: Text('Fauzan Abdillah'),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage('assets/img/profile.jpg'),
            ),
            accountEmail: Text('fauzan.abdillah2705@gmail.com'),
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: 'Beranda',
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.account_circle,
            title: 'Profil',
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.manage_accounts,
            title: 'Perbarui Profil',
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.credit_card,
            title: 'Top Up Saldo',
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.store,
            title: 'Toko',
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.shopping_cart,
            title: 'Keranjang',
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.event_note,
            title: 'Transaksi',
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.history,
            title: 'Riwayat Transaksi',
            onTilePressed: () {},
          ),
          const Divider(),
          DrawerListTile(
            iconData: Icons.settings,
            title: 'Settings',
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.call,
            title: 'Hubungi Kami',
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.help_outline,
            title: 'Bantuan?',
            onTilePressed: () {},
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;

  const DrawerListTile({
    Key? key,
    this.iconData,
    this.title,
    this.onTilePressed,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title.toString(),
        style: const TextStyle(
          fontSize: 16,
        ),
      ),
    );
  }
}
