import 'package:flutter/material.dart';
import 'package:minggu_5_flutter/drawer.dart';
import 'package:minggu_5_flutter/pages/first_page.dart';
import 'package:minggu_5_flutter/pages/third_page.dart';
import 'package:minggu_5_flutter/pages/second_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int currentIndex = 0;

  Widget body() {
    switch (currentIndex) {
      default:
        return const Center(
          child: Text(
            '404 | Page Not Found!',
            style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.bold,
            ),
          ),
        );
    }
  }

  // PENERAPAN BOTTOM NAVIGATION
  Widget customBottomNav() {
    return ClipRRect(
      borderRadius: const BorderRadius.vertical(
        top: Radius.circular(10),
      ),
      child: BottomAppBar(
        child: BottomNavigationBar(
          backgroundColor: Colors.red,
          selectedItemColor: Colors.white,
          currentIndex: currentIndex,
          onTap: (value) {
            // print(value);
            setState(
              () {
                currentIndex = value;
              },
            );
          },
          type: BottomNavigationBarType.fixed,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Container Page',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.directions),
              label: 'Route Page',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.disabled_by_default),
              label: 'Default Page',
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget container() {
      return Column(
        // PENERAPAN COLUMN (VERTICAL)
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.all(12),
            height: 50,
            width: 50,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.red,
            ),
          ),
          Container(
            margin: const EdgeInsets.all(12),
            height: 50,
            width: 50,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.yellow,
            ),
          ),
          Container(
            margin: const EdgeInsets.all(12),
            height: 50,
            width: 50,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.green,
            ),
          ),

          // PENERAPAN PENGGUNAAN ROW (HORIZONTAL)
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                margin: const EdgeInsets.all(12),
                height: 50,
                width: 50,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,
                ),
              ),
              Container(
                margin: const EdgeInsets.all(12),
                height: 50,
                width: 50,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.blue,
                ),
              ),
              Container(
                margin: const EdgeInsets.all(12),
                height: 50,
                width: 50,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.amber,
                ),
              ),
            ],
          ),
        ],
      );
    }

    Widget navigation() {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // PENERAPAN MATERIAL PAGE ROUTE
          Builder(
            builder: (context) => Center(
              child: ElevatedButton(
                onPressed: () {
                  Route route = MaterialPageRoute(
                    builder: (context) => const FirstPage(),
                  );
                  Navigator.push(context, route);
                },
                child: const Text('Tap Untuk ke FirstPage'),
              ),
            ),
          ),
          const SizedBox(height: 12),

          // PENERAPAN ROUTE MENGGUNAKAN NAME ROUTE
          Builder(
            builder: (context) => Center(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/second-page');
                },
                child: const Text('Tap Untuk ke Second Page'),
              ),
            ),
          ),
          const SizedBox(height: 12),

          // PENERAPAN ROUTE GENERATOR
          Builder(
            builder: (context) => Center(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/third-page');
                },
                child: const Text('Tap Untuk ke Third Page'),
              ),
            ),
          ),
        ],
      );
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // UNTUK APPBAR
        appBar: AppBar(
          title: const Text('E41201367 - Fauzan Abdillah'),
          actions: const [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Icon(Icons.search),
            ),
          ],
          actionsIconTheme: const IconThemeData(color: Colors.white),
          centerTitle: true,
          backgroundColor: Colors.red,
          bottom: PreferredSize(
            child: Container(
              color: Colors.white,
              height: 4.0,
            ),
            preferredSize: const Size.fromHeight(1.0),
          ),
          elevation: 10,
        ),

        // PENERAPAN FLOATING BUTTON
        floatingActionButton: FloatingActionButton(
          child: const Text('+'),
          onPressed: () {},
          backgroundColor: Colors.red,
        ),

        // PENERAPAN PENGGUNAAN COLUMN (VERTICAL)
        body: currentIndex == 0
            ? container()
            : currentIndex == 1
                ? navigation()
                : body(),

        // PENERAPAN DRAWER SCREEN PAGE
        drawer: DrawerScreenPage(),

        // PENERAPAN BOTTOM NAVIGATION
        bottomNavigationBar: customBottomNav(),
      ),

      // PENERAPAN ROUTE MENGGUNAKAN NAME ROUTE
      routes: <String, WidgetBuilder>{
        '/second-page': (BuildContext context) => const SecondPage(),
        '/third-page': (BuildContext context) => const ThirdPage(),
      },
    );
  }
}
