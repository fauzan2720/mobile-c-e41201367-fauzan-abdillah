import 'package:flutter/material.dart';
import 'package:minggu_5_flutter/main.dart';
import 'package:minggu_5_flutter/pages/third_page.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => const MyApp());
      case '/third-page':
        return MaterialPageRoute(builder: (context) => const ThirdPage());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (context) {
        return Scaffold(
          appBar: AppBar(title: const Text("Error")),
          body: const Center(child: Text('Error page')),
        );
      },
    );
  }
}
