# minggu_6_gradient_pageview_dropdown

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Output

1. Gradient Color (Linear Gradient, Radial Gradient, and Sweep Gradient)

![output1](https://user-images.githubusercontent.com/74108522/160876444-a2f35f61-24cd-4cde-8ba3-f82386ececb1.png)

2. Page View (Basketball Players)

![output2 1](https://user-images.githubusercontent.com/74108522/160876452-499e5538-6f25-40a7-a74d-148c55328800.png)
![output2 2](https://user-images.githubusercontent.com/74108522/160876456-941bfa48-f9b8-4e3f-b51b-d1dbbc3afef5.png)

3. Dropdown Menu

![output3](https://user-images.githubusercontent.com/74108522/160876459-ac16d0ac-9d53-4689-8081-c710ca745b55.png)