import 'package:flutter/material.dart';

class DrowpdownPage extends StatefulWidget {
  const DrowpdownPage({Key? key}) : super(key: key);

  @override
  State<DrowpdownPage> createState() => _DrowpdownPageState();
}

class _DrowpdownPageState extends State<DrowpdownPage> {
  String dropdownValue = 'Pilih Program Studi';

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(30),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('Pilih Program Studi Anda: '),
          const SizedBox(height: 10),

          // DROPDOWN
          DropdownButton<String>(
            value: dropdownValue,
            icon: const Icon(Icons.arrow_downward),
            elevation: 16,
            style: const TextStyle(color: Colors.black),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            onChanged: (String? index) {
              setState(() {
                dropdownValue = index!;
              });
            },
            items: <String>[
              'Pilih Program Studi',
              'D4 Teknik Informatika',
              'D3 Manajemen Informatika',
              'D3 Teknik Komputer',
              'D4 Bisnis Digital',
            ].map<DropdownMenuItem<String>>(
              (String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              },
            ).toList(),
          ),
        ],
      ),
    );
  }
}
