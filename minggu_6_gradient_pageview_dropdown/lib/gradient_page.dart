import 'package:flutter/material.dart';

class GradientPage extends StatelessWidget {
  const GradientPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget linearGradient() {
      return Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color(0xff758ECD),
              Color(0xffC1CEFE),
              Color(0xff624CAB),
            ],
          ),
        ),
      );
    }

    Widget radialGradient() {
      return Container(
        decoration: const BoxDecoration(
          gradient: RadialGradient(
            radius: 0.8,
            colors: [
              Color(0xff758ECD),
              Color(0xffC1CEFE),
              Color(0xff624CAB),
            ],
          ),
        ),
      );
    }

    Widget sweepGradient() {
      return Container(
        decoration: const BoxDecoration(
          gradient: SweepGradient(
            startAngle: 0.4,
            endAngle: 0.7,
            colors: [
              Color(0xff624CAB),
              Color(0xff758ECD),
              Color(0xffC1CEFE),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      body: Column(
        children: [
          // LINEAR GRADIENT
          Flexible(
            flex: 1,
            child: linearGradient(),
          ),

          // RADIAL GRADIENT
          Flexible(
            flex: 2,
            child: radialGradient(),
          ),

          // SWEEP GRADIENT
          Flexible(
            flex: 1,
            child: sweepGradient(),
          ),
        ],
      ),
    );
  }
}
