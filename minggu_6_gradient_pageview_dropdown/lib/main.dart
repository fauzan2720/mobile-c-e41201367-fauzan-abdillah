import 'package:flutter/material.dart';
import 'package:minggu_6_gradient_pageview_dropdown/dropdown.dart';
import 'package:minggu_6_gradient_pageview_dropdown/gradient_page.dart';
import 'package:minggu_6_gradient_pageview_dropdown/pageview.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'E41201367 - Fauzan Abdillah',
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;

  static const List<Widget> _widgetOptions = <Widget>[
    GradientPage(),
    PageView_(),
    DrowpdownPage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('E41201367 - Fauzan Abdillah'),
        backgroundColor: const Color(0xff758ECD),
        elevation: 0,
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: const Color(0xff758ECD),
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.palette),
            label: 'Gradient',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.description),
            label: 'Page View',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Dropdown',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}
