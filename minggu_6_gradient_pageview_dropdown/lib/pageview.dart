import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:minggu_6_gradient_pageview_dropdown/second_page.dart';

class PageView_ extends StatefulWidget {
  const PageView_({Key? key}) : super(key: key);

  @override
  State<PageView_> createState() => _PageView_State();
}

class _PageView_State extends State<PageView_> {
  final List<String> images = [
    'img1.jpg',
    'img2.jpg',
    'img3.jpg',
    'img4.jpg',
    'img5.jpg',
    'img6.jpg',
    'img7.jpg',
    'img8.jpg',
  ];

  static const Map<String, Color> colors = {
    'chimmy': Color(0xff2db569),
    'cooky': Color(0xfff386b8),
    'koya': Color(0xff45caf5),
    'mang': Color(0xffb19ecb),
    'rj': Color(0xfff58e4c),
    'shooky': Color(0xff46c1be),
    'tata': Color(0xffffea0e),
    'van': Color(0xffdbe4e9),
  };

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;

    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xff758ECD),
              Color(0xffC1CEFE),
              Color(0xff624CAB),
            ],
          ),
        ),
        child: PageView.builder(
          controller: PageController(viewportFraction: 0.8),
          itemCount: images.length,
          itemBuilder: (BuildContext context, int i) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 50,
                horizontal: 5,
              ),
              child: Material(
                elevation: 8,
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    Hero(
                      tag: images[i],
                      child: Material(
                        child: InkWell(
                          child: Container(
                            color: colors.values.elementAt(i),
                            child: Image.asset(
                              'assets/img/${images[i]}',
                              fit: BoxFit.cover,
                            ),
                          ),
                          onTap: () => Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) => SecondPage(
                                images: images[i],
                                colors: colors.values.elementAt(i),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
