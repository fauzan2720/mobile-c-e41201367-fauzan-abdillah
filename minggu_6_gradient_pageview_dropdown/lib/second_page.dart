import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  const SecondPage({Key? key, required this.images, required this.colors})
      : super(key: key);
  final String images;
  final Color colors;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("E41201367 - Fauzan Abdillah"),
        backgroundColor: const Color(0xff624CAB),
        elevation: 0,
      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              gradient: RadialGradient(
                center: Alignment.center,
                colors: [
                  Color(0xff758ECD),
                  Color(0xffC1CEFE),
                  Color(0xff624CAB),
                ],
              ),
            ),
          ),
          Center(
            child: Hero(
              tag: images,
              child: ClipOval(
                child: SizedBox(
                  width: 200.0,
                  height: 200.0,
                  child: Material(
                    child: InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      child: Container(
                        color: colors,
                        child: Image.asset(
                          "assets/img/$images",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
