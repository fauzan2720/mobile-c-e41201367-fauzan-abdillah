import 'package:flutter/material.dart';

class CheckBoxAndSwitchButtonPage extends StatefulWidget {
  const CheckBoxAndSwitchButtonPage({Key? key}) : super(key: key);

  @override
  State<CheckBoxAndSwitchButtonPage> createState() =>
      _CheckBoxAndSwitchButtonPageState();
}

class _CheckBoxAndSwitchButtonPageState
    extends State<CheckBoxAndSwitchButtonPage> {
  bool? valueCheckBox = false;
  bool valueSwitchButton = true;
  double valueSlider = 10;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        checkBoxItem(),
        switchButtonItem(),
        sliderItem(),
      ],
    );
  }

  Widget checkBoxItem() {
    return CheckboxListTile(
      value: valueCheckBox,
      onChanged: (value) {
        setState(() {
          valueCheckBox = value;
        });
      },
      title: const Text('Check Box'),
      subtitle: const Text('Belajar Check Box Flutter'),
      activeColor: Colors.blue,
    );
  }

  Widget switchButtonItem() {
    return SwitchListTile(
      value: valueSwitchButton,
      onChanged: (value) {
        setState(() {
          valueSwitchButton = value;
        });
      },
      title: const Text('Switch Button'),
      subtitle: const Text('Belajar Switch Button Flutter'),
      activeColor: Colors.blue,
      activeTrackColor: Colors.blueGrey,
    );
  }

  Widget sliderItem() {
    return Slider(
      value: valueSlider,
      min: 0,
      max: 120,
      onChanged: (value) {
        setState(() {
          valueSlider = value;
        });
      },
      activeColor: Colors.deepOrangeAccent,
    );
  }
}
