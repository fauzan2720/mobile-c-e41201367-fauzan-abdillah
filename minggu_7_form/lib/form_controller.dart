import 'package:flutter/material.dart';

class FormControllerPage extends StatefulWidget {
  const FormControllerPage({Key? key}) : super(key: key);

  @override
  State<FormControllerPage> createState() => _FormControllerPageState();
}

class _FormControllerPageState extends State<FormControllerPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController motoController = TextEditingController();

  List<String> listReligion = [
    'Islam',
    'Kristen Protestan',
    'Kristen Katolik',
    'Hindu',
    'Budha',
  ];

  String religion = 'Islam';
  String gender = '';

  void selectReligion(String value) {
    setState(() {
      religion = value;
    });
  }

  void selectGender(String value) {
    setState(() {
      gender = value;
    });
  }

  void sendData() {
    AlertDialog alertDialog = AlertDialog(
      content: SizedBox(
        height: 200,
        child: Column(
          children: [
            Text('Nama Lengkap: ${nameController.text}'),
            Text('Password: ${passwordController.text}'),
            Text('Moto Hidup: ${motoController.text}'),
            Text('Jenis Kelamin: $gender'),
            Text('Agama: $religion'),
            Container(
              width: double.infinity,
              height: 40,
              margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
              child: TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text(
                  'OK',
                  style: TextStyle(color: Colors.white),
                ),
                style: TextButton.styleFrom(
                  backgroundColor: Colors.blue,
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    Widget nameInput() {
      return Container(
        margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
        child: TextFormField(
          controller: nameController,
          autofocus: true,
          keyboardType: TextInputType.name,
          decoration: InputDecoration(
            hintText: "ex: Fauzan Abdillah",
            labelText: "Nama Lengkap",
            icon: const Icon(Icons.people),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        ),
      );
    }

    Widget passwordInput() {
      return Container(
        margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
        child: TextFormField(
          controller: passwordController,
          obscureText: true,
          decoration: InputDecoration(
            hintText: "ex: Fauzan1234",
            labelText: "Password",
            icon: const Icon(Icons.key),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        ),
      );
    }

    Widget motoInput() {
      return Container(
        margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
        child: TextFormField(
          controller: motoController,
          maxLines: 3,
          decoration: InputDecoration(
            hintText: "ex: Never Give Up!",
            labelText: "Moto Hidup",
            icon: const Icon(Icons.person),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        ),
      );
    }

    Widget genderSelect() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 10),
            child: const Text(
              'Jenis Kelamin:',
              style: TextStyle(
                fontSize: 18,
              ),
            ),
          ),
          RadioListTile<String>(
            value: 'Laki-Laki',
            title: const Text('Laki-Laki'),
            groupValue: gender.toString(),
            onChanged: (String? value) {
              selectGender(value!);
            },
            activeColor: Colors.blue,
          ),
          RadioListTile<String>(
            value: 'Perempuan',
            title: const Text('Perempuan'),
            groupValue: gender.toString(),
            onChanged: (String? value) {
              selectGender(value!);
            },
            activeColor: Colors.blue,
          ),
        ],
      );
    }

    Widget religionDropdown() {
      return Row(
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 10),
            child: const Text(
              'Agama: ',
              style: TextStyle(fontSize: 18),
            ),
          ),
          DropdownButton(
            onChanged: (String? value) {
              selectReligion(value!);
            },
            value: religion,
            items: listReligion.map((String value) {
              return DropdownMenuItem(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ],
      );
    }

    Widget button() {
      return Container(
        width: double.infinity,
        height: 40,
        margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
        child: TextButton(
          onPressed: () {
            sendData();
          },
          child: const Text(
            'Submit',
            style: TextStyle(color: Colors.white),
          ),
          style: TextButton.styleFrom(
            backgroundColor: Colors.blue,
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        ),
      );
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: const Icon(Icons.list),
          title: const Text('Biodata Mahasiswa'),
          backgroundColor: Colors.teal,
        ),
        body: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(
            children: [
              nameInput(),
              passwordInput(),
              motoInput(),
              genderSelect(),
              religionDropdown(),
              button(),
            ],
          ),
        ),
      ),
    );
  }
}
