import 'package:flutter/material.dart';
import 'package:minggu_7_form/checkbox_and_switchbutton.dart';
import 'package:minggu_7_form/form_controller.dart';
import 'package:minggu_7_form/textformfield.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('E41201367 - Fauzan Abdillah'),
          actions: [
            Padding(
              padding: const EdgeInsets.all(8),
              child: Builder(
                builder: (context) {
                  return GestureDetector(
                    child: const Icon(Icons.send),
                    onTap: () {
                      Navigator.pushNamed(context, '/pert3');
                    },
                  );
                },
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: const [
              TextFormFieldPage(),
              SizedBox(height: 20),
              CheckBoxAndSwitchButtonPage(),
            ],
          ),
        ),
      ),
      routes: {
        '/pert3': (BuildContext context) => const FormControllerPage(),
      },
    );
  }
}
