import 'package:flutter/material.dart';

class TextFormFieldPage extends StatefulWidget {
  const TextFormFieldPage({Key? key}) : super(key: key);

  @override
  State<TextFormFieldPage> createState() => _TextFormFieldPageState();
}

class _TextFormFieldPageState extends State<TextFormFieldPage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          nameInput(),
          emailInput(),
          phoneNumberInput(),
          urlInput(),
          dateInput(),
          passwordInput(),
          button(),
        ],
      ),
    );
  }

  Widget nameInput() {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: TextFormField(
        autofocus: true,
        keyboardType: TextInputType.name,
        validator: (value) {
          if (value == '') {
            return 'Nama wajib diisi!';
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: "Masukan nama lengkap anda",
          labelText: "Nama Lengkap",
          icon: const Icon(Icons.people),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }

  Widget emailInput() {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        validator: (value) {
          if (value == '') {
            return 'Email wajib diisi!';
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: "Masukan email anda",
          labelText: "Email",
          icon: const Icon(Icons.email),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }

  Widget phoneNumberInput() {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: TextFormField(
        keyboardType: TextInputType.phone,
        validator: (value) {
          if (value == '') {
            return 'Nomor Telepon wajib diisi!';
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: "Masukan nomor telepon anda",
          labelText: "Nomor Telepon",
          icon: const Icon(Icons.phone),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }

  Widget urlInput() {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: TextFormField(
        keyboardType: TextInputType.url,
        decoration: InputDecoration(
          hintText: "Masukan link portofolio anda",
          labelText: "Link Portofolio",
          icon: const Icon(Icons.link),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }

  Widget dateInput() {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: TextFormField(
        keyboardType: TextInputType.datetime,
        decoration: InputDecoration(
          hintText: "Masukan tanggal lahir anda",
          labelText: "Tanggal Lahir",
          icon: const Icon(Icons.date_range),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }

  Widget passwordInput() {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: TextFormField(
        obscureText: true,
        validator: (value) {
          if (value == '') {
            return 'Password wajib diisi!';
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: "Masukan password anda",
          labelText: "Password",
          icon: const Icon(Icons.key),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }

  Widget button() {
    return Container(
      width: double.infinity,
      height: 40,
      margin: const EdgeInsets.fromLTRB(20, 30, 20, 0),
      child: TextButton(
        onPressed: () {
          if (_formKey.currentState!.validate()) {}
        },
        child: const Text(
          'Submit',
          style: TextStyle(color: Colors.white),
        ),
        style: TextButton.styleFrom(
          backgroundColor: Colors.blue,
          elevation: 2,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }
}
