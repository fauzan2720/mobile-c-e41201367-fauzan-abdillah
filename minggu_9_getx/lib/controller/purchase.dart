import 'package:get/get.dart';
import 'package:minggu_9_getx/model/product.dart';

class Purchase extends GetxController {
  var products = <Product>[].obs;
  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    await Future.delayed(const Duration(seconds: 1));

    // memanggil dari server
    var serverResponse = [
      Product(1, 'Demo Product', 'Fauzan',
          'Ini adalah contoh penggunaan Product menggunakan getX', 300),
      Product(1, 'Demo Product', 'Fauzan',
          'Ini adalah contoh penggunaan Product menggunakan getX', 300),
      Product(1, 'Demo Product', 'Fauzan',
          'Ini adalah contoh penggunaan Product menggunakan getX', 300),
      Product(1, 'Demo Product', 'Fauzan',
          'Ini adalah contoh penggunaan Product menggunakan getX', 300),
      Product(1, 'Demo Product', 'Fauzan',
          'Ini adalah contoh penggunaan Product menggunakan getX', 300),
      Product(1, 'Demo Product', 'Fauzan',
          'Ini adalah contoh penggunaan Product menggunakan getX', 300),
    ];

    products.value = serverResponse;
  }
}
