import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:minggu_9_getx/controller/demo_controller.dart';
import 'package:minggu_9_getx/view/demo_page.dart';
import 'package:minggu_9_getx/view/home.dart';

void main() async {
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final DemoController ctrl = Get.put(DemoController());

    return SimpleBuilder(
      builder: (_) {
        return GetMaterialApp(
          title: 'Easy GetX',
          theme: ctrl.theme,
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          routes: {
            //routes for named navigation
            '/': (context) => HomePage(),
            '/cart': (context) => DemoPage(),
          },
        );
      },
    );
  }
}
