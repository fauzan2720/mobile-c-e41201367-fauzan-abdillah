import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:minggu_9_getx/controller/demo_controller.dart';

class DemoPage extends StatelessWidget {
  DemoPage({Key? key}) : super(key: key);

  final DemoController ctrl = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Demo Page'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(Get.arguments),
          ),
          const SizedBox(height: 12),
          SwitchListTile(
            value: ctrl.isDark,
            onChanged: ctrl.changeTheme,
            title: const Text('Touch to change ThemeMode'),
          ),
          const SizedBox(height: 12),
          ElevatedButton(
            onPressed: () => Get.snackbar(
              'Snackbar',
              'Hello this is the Snackbar message',
              snackPosition: SnackPosition.BOTTOM,
              colorText: Colors.white,
              backgroundColor: Colors.black87,
            ),
            child: const Text('Snack Bar'),
          ),
          const SizedBox(height: 12),
          ElevatedButton(
            onPressed: () => Get.defaultDialog(
              title: 'Dialogue',
              content: const Text('Heyy, From Dialogue'),
            ),
            child: const Text('Dialogue'),
          ),
          const SizedBox(height: 12),
          ElevatedButton(
            onPressed: () => Get.bottomSheet(
              Container(
                height: 150,
                color: Colors.white,
                child: const Text('Hello, From Bottom Sheet',
                    style: TextStyle(fontSize: 30)),
              ),
            ),
            child: const Text('Bottom Sheet'),
          ),
          const SizedBox(height: 12),
          ElevatedButton(
            onPressed: () => Get.offNamed('/'),
            child: const Text('Back To Home'),
          ),
        ],
      ),
    );
  }
}
